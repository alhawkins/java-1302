import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

/**
 	Author: Allan Hawkins 
	Class: CSCI 1302   
	This class is an applet that draws a house 
	and lets the user close the doors and wondows to the house.
*/

public class House extends JApplet
{
   // create boolean variables
	private boolean leftWindow;
	private boolean door;
	private boolean rightWindow;
	
	/**
      init method
   */
   
   public void init()
   {
  		// set boolean variables to true
		leftWindow = true;
		door = true;
		rightWindow = true;
      
		// Set the background color to white.
      getContentPane().setBackground(Color.white);
   
		// Add a mouse listener to this applet.
      addMouseListener(new MyMouseListener());
	}
   
   /**
      paint method
      @param g The applet's Graphics object.
   */
   
   public void paint(Graphics g)
   {
      // Call the superclass paint method.
      super.paint(g);
      
  		    
		// Draw a black unfilled rectangle.
      g.setColor(Color.black);
      g.drawRect(50, 150, 300, 150);
      
		// Draw a black line for the roof.
      g.setColor(Color.black);
      g.drawLine(30, 150, 200, 40);
		
		// Draw a black line for the roof.
      g.setColor(Color.black);
      g.drawLine(200, 40, 370, 150);
		
		// Draw a black line for the roof.
      g.setColor(Color.black);
      g.drawLine(30, 150, 370, 150);
				
		if (leftWindow)
		{	
			// Draw a black filled left window.
	      g.setColor(Color.black);
	      g.fillRect(85, 200, 50, 50);
		}
		
		if (!(leftWindow))
		{	
			// Draw left window frame.
	      g.setColor(Color.black);
	      g.drawRect(85, 200, 50, 50);
			g.drawLine(85, 225, 135, 225);
			g.drawLine(110, 200, 110, 250);
			
		}

		if (door)
		{    
			// Draw a black filled door.
	      g.setColor(Color.black);
	      g.fillRect(175, 200, 50, 100);
		}
		
		if (!(door))
		{
			// Draw a Door with a knob
			g.setColor(Color.black);
			g.drawRect(175, 200, 50,100);
			g.fillOval(215, 260, 6, 6);
			
		}
		
		if (rightWindow)
		{	
			// Draw a black filled right window.
	      g.setColor(Color.black);
	      g.fillRect(265, 200, 50, 50);
	   }
		
		if (!(rightWindow))
		{	
			// Draw a right window frame.
	      g.setColor(Color.black);
	      g.drawRect(265, 200, 50, 50);
			g.drawLine(265, 225, 315, 225);
			g.drawLine(290, 200, 290, 250);
			
		}

	}
		
	
		private class MyMouseListener implements MouseListener
		{
			public void mousePressed(MouseEvent e)
	      {

	      }
	
	      public void mouseClicked(MouseEvent e)
	      {
	   		// variables that store x and y coordinates   	
				int x = e.getX();
				int y = e.getY();   
				
				// Set the left window restrictions for the x and y coordinates
				boolean lWindow = (x >= 85 && x <= 135 && y >= 200 && y <= 250);
				
					// If condition that calls repaint method
					if (lWindow)
					{
						leftWindow = !leftWindow;
						repaint();
					}
					
				// Set the door restrictions for the x and y coordinates					
				boolean door2 = (x >= 175 && x <= 225 && y >= 200 && y <= 300);
				   
					// If condition that calls repaint method
					if (door2) 
					{
						door = !door;
						repaint();
					}
				
				// Set the right window restrictions for the x and y coordinates	
				boolean rWindow = (x >= 265 && x <= 315 && y >= 200 && y <= 250);
				
	            // If condition that calls repaint method	
					if (rWindow)
					{
						rightWindow = !rightWindow;
						repaint();
					}

    
	      }
	
	      public void mouseReleased(MouseEvent e)
	      {
	         
	      }
	
	      public void mouseEntered(MouseEvent e)
	      {
	         
	      }
	
	      public void mouseExited(MouseEvent e)
	      {
	         
	      }
		}

}
