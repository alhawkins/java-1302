import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/** This class demonstrates an applet with house drawing
	 the door and windows will close, when the user click on the door or windows.
*/

public class HouseApplet extends JApplet
{
	boolean winleft, winright, door;


	/*
		init method
	*/
	public void init()
	{
		winleft = winright = door = true;	// Set the boolean to false.
		
		// Set the background color to white.
		getContentPane().setBackground(Color.white);
		
		//Add a mouse listener class to this applet.
		addMouseListener(new MyMouseListener());
		
	}
	
	/**
		paint method
		@param g The applet's Graphics object.
	*/
	
	public void paint(Graphics g)
	{
		// Call the superclass paint method.
		super.paint(g);
		
					
		if (winleft) 
		{
				g.fillRect(50, 55, 40, 40);
		}
			
		if (winright) 
		{
				g.fillRect(140, 55, 40, 40);
		}
			
		if (door) 
		{
				g.fillRect(105, 55, 20, 50);
		}
		
				
		// Draw the triangle roof from (5,115) to (45,20)
		// and (5,115) to (45,210).
		g.setColor(Color.black);
		g.drawLine(20, 45, 115, 5);
		g.drawLine(115, 5, 210, 45);
		g.drawLine(20,45, 210, 45);
		
		// Draw the rectangle for the house base.
		g.drawRect(30, 45, 170, 60);
		
		// Draw the left window frame & the window.
		g.drawRect(50, 55, 40, 40);
		g.drawLine(70, 55, 70, 95);
		g.drawLine(50, 75, 90, 75);
		
		// Draw the right window frame & the window.
		g.drawRect(140, 55, 40, 40);
		g.drawLine(160, 55, 160, 95);
		g.drawLine(140, 75, 180, 75);

		// Draw the Door.
		g.drawRect(105, 55, 20,50);
		
		// Draw a gold filled door lock.
		g.setColor(Color.blue);
		g.fillOval(117, 80, 5, 5);
	}

	private class MyMouseListener implements MouseListener
	{
			public void mousePressed(MouseEvent e)
			{
			}
			
			public void mouseClicked(MouseEvent e)
			{
				int currentx = e.getX();
				int currenty = e.getY();

				//Set the loop to find which area the click event occured
				//if event occurs, sets the boolean to false and call the repaint method
				boolean WindowLeft = (currentx >= 50 && currentx < 90 && currenty >= 55 && currenty <= 95);
				if (WindowLeft)
				{
					winleft = false;
					repaint();
				}
				
									
				boolean Door = (currentx >= 105 && currentx < 125 && currenty >= 55 && currenty <= 105);
				if (Door) {
					door = false;
					repaint();
				}
				
				boolean WindowRight = (currentx >= 140 && currentx < 180 && currenty >= 55 && currenty <= 95);
				if (WindowRight)
				{
					winright = false;
					repaint();
				}

				else;
			}

			public void mouseReleased(MouseEvent e)
			{
			}

			public void mouseEntered(MouseEvent e)
			{
			}

			public void mouseExited(MouseEvent e)
			{
			}
	}
}

