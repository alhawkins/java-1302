import javax.swing.JOptionPane;

public class TestScores
{
	private double[] scores;

	public TestScores(double[] sco)
	{
		scores = sco;
	} 
	
	public void averageScore()
	{
	double total = 0;
	double aveScore = 0;
	
	for (int i=0; i<4; i++)
		{
			total+= scores[i];		
		
			if(scores[i] > 100)
			{
				throw new IllegalArgumentException("Error: Score " + (i+1) + " is over 100. Reenter score.");
			}
						
			if(scores[i] < 0)
			{
				throw new IllegalArgumentException("Error: Score " + (i+1) + "is under 0. Reenter score.");
			}
		}
		aveScore = total/4;
		
		JOptionPane.showMessageDialog(null, "Your average score is: " + aveScore);
	}
} 