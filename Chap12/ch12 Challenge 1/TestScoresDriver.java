import javax.swing.JOptionPane;

public class TestScoresDriver
{
	public static void main(String[] args)
	{
		double[] scr = new double[4];
		String[] input = new String[4];
		
		for(int i=0; i<4; i++)
		{
			input[i] = JOptionPane.showInputDialog("Please enter score# " + (i+1));
			scr[i] = Double.parseDouble(input[i]);
		}	
		
		TestScores test = new TestScores(scr);
		test.averageScore();
	}
}