import javax.swing.JOptionPane;

/**
	Author: Allan Hawkins 
	Class: CSCI 1302 
	This class demonstrates the use of a custom exception
*/
public class TestScores2
{
	// Arry to hold user information 
	private double[] scores;

	public TestScores2(double[] sco) throws InvalidTestScore
	{
		// Loop that test the array 
		for (int i=0; i<4; i++)
		{		
		
			if(sco[i] > 100 || sco[i] < 0)
			{
				throw new InvalidTestScore();
			}
						
		}
		scores = sco;
	} 
	
	// Constructor with no parameter 
	public TestScores2()
	{
		
	} 

	// class method 
	public void averageScore()
	{
	double total = 0;
	double aveScore = 0;
	
	for (int i=0; i<4; i++)
		{
			total+= scores[i];				
		}
		aveScore = total/4;
		
		JOptionPane.showMessageDialog(null, "Your average score is: " + aveScore);
	}
} 