import javax.swing.JOptionPane;

/**
	Author: Allan Hawkis 
	Class: CSCI 1302
	This program test a custom exception
*/
public class TestScores2Driver
{
	
	public static void main(String[] args)
	{
		// Arrays to hold the users information
		double[] scr = new double[4];
		String[] input = new String[4];
		
		// Loop to gather information from the user
		for(int i=0; i<4; i++)
		{
			input[i] = JOptionPane.showInputDialog("Please enter score# " + (i+1));
			scr[i] = Double.parseDouble(input[i]);
		}	
		
		// Try statement to test the constructor 
		try
		{
		TestScores2 test = new TestScores2(scr);
		}
		catch(InvalidTestScore e)
		{
			System.out.println(e.getMessage());
		}
		
		// Creates an instance of the TestScores2 class in order to use its methods
		TestScores2 test = new TestScores2();
		test.averageScore();
	}
}