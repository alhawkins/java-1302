/**
 	Author: Allan Hawkins 
	Class: CSCI 1302  
	
	InvalidTestScore exceptions are thrown by the
   TestScores2 class when a score, that is negative or over 
	a 100, is passed to the constructor.
*/

public class InvalidTestScore extends Exception
{
   /**
      This constructor prints out an
      error message.
   */

 	public InvalidTestScore()
   {
      super("Error: You have entered a score less then zero or" +
				" over a hundred. Reenter scores.");
   }
  	   
}
