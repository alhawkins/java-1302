import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 	Author: Allan Hawkins 
	Class: CSCI 1302  
	This class calculates the dorm and Meal plan charges.
*/

public class DormMealCost extends JFrame
{
   private JPanel dormCostPanel;  // To hold components
   private JPanel mealCostPanel;  // To hold components  
	private JPanel totalCostPanel; // To hold components
   private JComboBox dormBox;     // A list of dorms
   private JComboBox mealBox;     // A list of meals  
	private JLabel label;          // Displays a message
	private JTextField totalCost;  // Displays the total charges for the dorm and meal plan

   // The following array holds the values that will
   // be displayed in the dormBox combo box.
   private String[] dorms = { "","Allen Hall: $1,500 per semester",
                     "Pike Hall: $1,600 per semester", "Farthing Hall: $1,200 per semester",
                     "University Suites: $ 1,800 per semester",};
	
	// Array that holds charges per dorm
	private int[] dormCost = {0,1500,1600,1200,1800};
	
	// The following array holds the values that will
   // be displayed in the mealBox combo box.
   private String[] meals = { "","7 meals per week: $560 per semester",
                     "14 meals per week: $1,095 per semester", "Unlimited meals: $1,500",};
 
   // Array that holds charges per plan
	private int[] mealCost = {0,560,1095,1500};
	
   /**
      Constructor
   */

   public DormMealCost()
   {
      // Set the title.
      setTitle("Dorm and Meal Plan Calculator");

		    
		// Specify an action for the close button.
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

      // Create a BorderLayout manager.
      setLayout(new BorderLayout());

      // Build the panels.
      buildDormCostPanel();
  		buildMealCostPanel();    
		buildTotalCostPanel();

      // Add the panels to the content pane.
      add(dormCostPanel, BorderLayout.WEST);
     	add(mealCostPanel, BorderLayout.EAST);
 		add(totalCostPanel, BorderLayout.SOUTH);

      // Pack and display the window.
      pack();
      setVisible(true);
   }

   /**
      The buildDormCostPanel method adds a combo box 
      with the different dorms to a panel.
   */

   private void buildDormCostPanel()
   {
      // Create a panel to hold the combo box.
      dormCostPanel = new JPanel();

 		// Create the label 	
		label = new JLabel("Select a Dorm:");
   
	   // Create the combo box
      dormBox = new JComboBox(dorms);
 
      // Register an action listener.
      dormBox.addActionListener(new ComboBoxListener());

      // Add the label and combo box to the panel.
      dormCostPanel.add(label);    
	   dormCostPanel.add(dormBox);
 		
	}

   /**
      The buildMealCostPanel method adds a combo box 
      with the different meal plans to a panel.
   */

   private void buildMealCostPanel()
   {
      // Create a panel to hold the combo box.
      mealCostPanel = new JPanel();

  		// Create the label		
		label = new JLabel("Select a Meal Plan:");
    
		// Create the combo box
      mealBox = new JComboBox(meals);

      // Register an action listener.
      mealBox.addActionListener(new ComboBoxListener());

      // Add the label and combo box to the panel.
      mealCostPanel.add(label);
		mealCostPanel.add(mealBox);
   }

	
	
	/**
      The buildTotalCostPanel method adds a
      read-only text field to a panel.
   */

   private void buildTotalCostPanel()
   {
      // Create a panel to hold the components.
      totalCostPanel = new JPanel();

      // Create the label.
      label = new JLabel("Total charges for the semester: ");

      // Create the uneditable text field.
      totalCost = new JTextField(10);
      totalCost.setEditable(false);

      // Add the label and text field to the panel.
      totalCostPanel.add(label);
      totalCostPanel.add(totalCost);
   }

   /**
      Private inner class that handles the event when
      the user selects the item from the combo box.
   */

   private class ComboBoxListener
                      implements ActionListener
   {

		   
		public void actionPerformed(ActionEvent e)
      {

			// Get the selected coffee.
         int index = dormBox.getSelectedIndex();
         int index2 = mealBox.getSelectedIndex();
         int total = dormCost[index] + mealCost[index2];
		

         
			// Display the selected coffee in the text field.
         totalCost.setText("$"+Integer.toString(total));
      }
   }

   /**
      The main method creates an instance of the
      DormMealCost class which causes it to display
      its window.
   */
   
   public static void main(String[] args)
   {
      DormMealCost cbw = new DormMealCost();
   }
}
