import javax.swing.*;    
import java.awt.event.*; 

public class MonthlySalesTaxWindow extends JFrame
{
   private JPanel panel;             
   private JLabel messageLabel;      
   private JTextField taxTextField; 
   private JButton calcButton;       
   private final int WINDOW_WIDTH = 420;  
   private final int WINDOW_HEIGHT = 100; 

   
   public MonthlySalesTaxWindow()
   {     
      setTitle("Monthly Sales Tax Calculator");
      
      setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
      
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
     
      buildPanel();
      
      add(panel);
      
      setVisible(true);
   }

   
   private void buildPanel()
   {
      
      messageLabel = new JLabel("Enter the amount to calculate. Do not include commas. i.e. 1000");

      taxTextField = new JTextField(10);
      
      calcButton = new JButton("Calculate");
      
      calcButton.addActionListener(new CalcButtonListener());
      
      panel = new JPanel();
     
      panel.add(messageLabel);
      panel.add(taxTextField);
      panel.add(calcButton);
   }

   

   private class CalcButtonListener implements ActionListener
   {
      public void actionPerformed(ActionEvent e)
      {
         String input;  
         double countyTax;  
			double stateTax;
        	double totalTax;
			double cTax = 0.02;
			double sTax = 0.04;
			
         input = taxTextField.getText();
       
         countyTax = Double.parseDouble(input) * cTax;
			stateTax = Double.parseDouble(input) * sTax;
			totalTax = countyTax + stateTax;
         
         JOptionPane.showMessageDialog(null,
                  "County sales tax is: " + countyTax + 
						"\nState sales tax is: " + stateTax +
						"\nTotal sales tax is: " + totalTax);

      }
   } 
	
   public static void main(String[] args)
   {
      MonthlySalesTaxWindow tc = new MonthlySalesTaxWindow();
   }
}