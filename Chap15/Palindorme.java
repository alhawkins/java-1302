import javax.swing.JOptionPane;

/**
	This class reads a word or a phrase entered by 
	the user, and analyses it to determine if it is 
	a palindrome
*/

public class Palindorme
{
	public static void main(String[] args)
	{
		String input; // To hold lower case users input 
		String input1; // To hold user input
		// Get word or frase from user
		input1 = JOptionPane.showInputDialog("Enter a word or phrase to determine if it is a Palindrome");
		input = input1.toLowerCase();
		
		// Displaye a message informint the user if the result
		if(Palin(input))
		{
			JOptionPane.showMessageDialog(null, "This is a Palindrome");
		}
		else
		{
			JOptionPane.showMessageDialog(null, "This is not a Palindrome");
		}	
		System.exit(0);
	}

	private static boolean Palin(String phrase)
	{
		boolean result = false;
		int left;
		int right = phrase.length() -1;
		
		if(phrase.length() == 1)
      {
			result = true;
		}
		
      else 
		{
      	for(left = 0; left < phrase.length(); left++, right--) 
			{
         	if(phrase.charAt(left) == phrase.charAt(right)) 
				{
    	        result = true;
     		   }
            
				else
				{
   		   	result = false;
    			}        
			}
   	}
        
        return result;
	}
}