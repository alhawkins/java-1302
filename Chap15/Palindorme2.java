
import javax.swing.JOptionPane;

/**
	Author: Allan Hawkins 
	Class: CSCI 1302

	This class reads a word or a phrase entered by 
	the user, and analyses it to determine if it is 
	a palindrome
*/

public class Palindorme2
{
	public static void main(String[] args)
	{
		String input; // To hold user input with no non-alphabetic characters 
		String input1; // To hold a lower case version of the user input
		String input2; // To hold user input 
		int length;
		
		// Get word or frase from user
		input2 = JOptionPane.showInputDialog("Enter a word or phrase to determine if it is a Palindrome");
		input1= input2.toLowerCase();
		input = input1.replaceAll("");
		length = input.length();
		
		// Displays a message informing the user of the result
		if(Palin(input, length))
		{
			JOptionPane.showMessageDialog(null, "This is a Palindrome");
		}
		else
		{
			JOptionPane.showMessageDialog(null, "This is not a Palindrome");
		}	
		System.exit(0);
	}

	// Recursion Method that analyses the string
	
	private static boolean Palin(String phrase, int i)
	{
		int k = i; // counter
		boolean result = false; // Boolean variable
		int left = 0;	// index for the left side of the phrase 
		int right = phrase.length() -1; // Index for the right side of the phrase
		
		// Condition to check if only one charater was entered. This will confirme a Palindrome
		if(phrase.length() == 1)
      {
			result = true;
		}
		
      // Recursion loop to check the user input.
      if((phrase.charAt(left) == phrase.charAt(right)) && !(k==0)) 
		{
    		left++;
			right--; 			       
   		Palin(phrase, k - 1);  
			result = true; 
	  	}
            
		else
		{
   		result = false;
    	}        
			       
   return result;
 
	}
}