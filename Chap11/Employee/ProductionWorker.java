/** 
	Author: Allan Hawkins
	Class: CSCI 1302 
	The ProductionWorker Class holds additional employee information 
*/

public class ProductionWorker extends Employee
{
	private int shift;       // Employee shift
   private double rate;   // Employee hourly pay rate

   /**
      The Constructor sets the employee's name,
      ID number, and hire date.
      @param n The employee's name.
      @param id The employee's ID number.
      @param date The date the empolyee was hired.
   */

 	public ProductionWorker(String n, String id, String date)
   {
      super(n,id,date);
   }
  
	public void setShift(int s)
	{
		shift = s;
	}
	
	public void setRate(int r)
	{
		rate = r;
	}

	/**
      The toString method returns a String containing
      the employee's data.
      @return A reference to a String.
   */

   public String toString()
   {
      String str;

      str = super.toString() 
			+ "\nWork Shift: " + shift
			+ "\nHourly Pay Rate: " + rate;
      return str;
   }

}