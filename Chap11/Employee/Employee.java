/**
 	Author: Allan Hawkins 
	Class: CSCI 1302  
	The Employee class holds
   general data about an Employee. 
*/

public class Employee
{
   private String name;       // Employee name
   private String idNumber;   // Employee ID
   private String hireDate;  // hire date

   /**
      The Constructor sets the employee's name,
      ID number, and hire date.
      @param n The employee's name.
      @param id The employee's ID number.
      @param date The date the empolyee was hired.
   */

   public Employee(String n, String id, String date)
   {
      name = n;
      idNumber = id;
      hireDate = date;
   }

   /**
      The toString method returns a String containing
      the employee's data.
      @return A reference to a String.
   */

 	public String getId()
	{
	
  		String id = "";
		    
		// Validate the idNumber.
      if (isValid(idNumber))
      {
         id = idNumber;
      }
      else
      {
         id ="That is not the proper format of a " +
             "employee number. Here is an " +
             "example: 345-A";
      }
      
     return id;
   }

   /**
      The isValid method determines whether a
      String is a valid customer number. If so, it
      returns true.
      @param empNumber The String to test.
      @return true if valid, otherwise false.
   */

   private static boolean isValid(String empNumber)
   	{
      	boolean goodSoFar = true;  // Flag
      	int i = 0;                 // Control variable

      	// Test the length.
      	if (empNumber.length() != 5)
       	  goodSoFar = false;

      	// Test the first three characters for digits.
      	while (goodSoFar && i < 3)
      	{
        	 if (!Character.isDigit(empNumber.charAt(i)))
        	    goodSoFar = false;
       	  i++;
      	}

  			// Test the 4th character for the special charater -.
     		 while (goodSoFar && i < 4)
      	{
     	    if (!(empNumber.charAt(i)== '-'))
    	        goodSoFar = false;
   	      i++;
  	   	}
    
			// Test the last character for leters.
      	while (goodSoFar && i < 5)
      	{
        	 if (!Character.isLetter(empNumber.charAt(i)))
       	     goodSoFar = false;
      	   i++;
     		}

    	  return goodSoFar;
   	}
	  
	
	public String toString()
   {
      String str;

      str = "Name: " + name
         + "\nID Number: " + getId()
         + "\nDate Hired: " + hireDate;
      return str;
   }

}