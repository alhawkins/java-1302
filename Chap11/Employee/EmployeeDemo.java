/** 
	Author: Allan Hawkins
	Class: CSCI 1302 
	The EmployeeDemo program demostrates the Employee 
	and ProductionWorker class
*/

public class EmployeeDemo
{
	public static void main(String[] args)
	{
		// ProductionWorker object
		ProductionWorker worker = new ProductionWorker("Allan Hawkins", "234=a","12/25/2010");
	
		// Store info on shift and rate
		worker.setShift(1);
		worker.setRate(20);
		
		// Display the employee's data
		System.out.println(worker);
	}
}
/**
	  
       ----jGRASP exec: java EmployeeDemo
    Name: Allan Hawkins
    ID Number: That is not the proper format of a employee number. Here is an example: 345-A
    Date Hired: 12/25/2010
    Work Shift: 1
    Hourly Pay Rate: 20.0
    
     ----jGRASP: operation complete.
    
*/